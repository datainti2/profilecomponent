import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
public w_name : string = "ebdesk Malaysia";
public max_topics : number = 100;
public max_fb : number = 100;
public max_twit : number = 100;
public max_blogs : number = 100;
public max_webs : number = 100;
public max_forums : number = 100;
public max_comparison : number = 100;
public max_comparison_twit : number = 0;
public max_comparison_fb : number = 0;
public max_users : number = 100;
  constructor() { }

  ngOnInit() {
  }

}
